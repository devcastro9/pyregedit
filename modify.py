import winreg as wr
import platform as pt
import ctypes, sys
"""
Programa para cambiar el servidor en la fuente de origen de datos
"""
def changeServer(subkey_server = ''):
    """ Cambia el servidor """
    status_changed = False
    result = ''
    try:
        with wr.ConnectRegistry(None,  wr.HKEY_LOCAL_MACHINE) as hkey:
            with wr.OpenKey(hkey, subkey_server, 0, wr.KEY_ALL_ACCESS) as subkey:
                wr.SetValueEx(subkey, 'Server', 0, wr.REG_EXPAND_SZ, '192.168.3.133')
                result = wr.EnumValue(subkey, 1)[0] + ' = ' + wr.EnumValue(subkey, 1)[1]
        status_changed = True
    except Exception as ex:
        print('Ha ocurrido un error al cambiar el servidor: ', ex)
    finally:
        return status_changed, result

def exist_skey(subkey_testing = ''):
    """ Verifica si existe la subkey en regedit """
    status_exists = False
    result = 'No existe'
    try:
        with wr.ConnectRegistry(None,  wr.HKEY_LOCAL_MACHINE) as hkey:
            with wr.OpenKey(hkey, subkey_testing, 0, wr.KEY_ALL_ACCESS) as subkey:
                result = wr.EnumValue(subkey, 1)[0] + ' = ' + wr.EnumValue(subkey, 1)[1]
        status_exists = True    
    except:
        pass
    finally:
        return status_exists, result

def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin() == 1
    except: 
        return False

def main():
    """ Code Main """
    subkey = (r"SOFTWARE\ODBC\ODBC.INI\ODBC_ADMIN_EMPRESA", r"SOFTWARE\WOW6432Node\ODBC\ODBC.INI\ODBC_ADMIN_EMPRESA")
    existe = [False, False]
    status = [False, False]
    result_old = ['', '']
    result = ['', '']
    # Inicio del procedimiento
    with open('odbc.log', 'a') as tlog:
        tlog.write('Sistema detectado:\n')
        tlog.write(pt.system() + ' ' + pt.release() + '\nVersion: ' + pt.version() + ' Edicion: ' + pt.win32_edition() + '\n')
        tlog.write('Arquitectura: ' + pt.architecture()[0] + '\n')
        if not is_admin():
            tlog.write('No se ejecuto este programa como administrador, no se hara ningun cambio.\n')
            sys.exit(0)
        if pt.architecture()[0] == '64bit':
            # Caso 64 bits
            for i in range(0, 2):
                existe[i], result_old[i] = exist_skey(subkey[i])
                if existe[i]:
                    status[i], result[i] = changeServer(subkey[i])
                    if status[i]:
                        tlog.write(f'Modificacion en ODBC {64 if i == 0 else 32} bits:\n')
                        tlog.write(result_old[i] + ' cambiado a: ' + result[i] + '\n')
                    else:
                        tlog.write(f'No se pudo cambiar en ODBC {64 if i == 0 else 32} bits.\n')
                else:
                    tlog.write(f'No se encontro la key en ODBC {64 if i == 0 else 32} bits.\n')
        else:
            # Caso 32 bits
            existe[0], result_old[0] = exist_skey(subkey[0])
            if existe[0]:
                status[0], result[0] = changeServer(subkey[0])
                if status[0]:
                    tlog.write('Modificacion en ODBC 32 bits:\n')
                    tlog.write(result_old[0] + ' cambiado a: ' + result[0] + '\n')
                else:
                    tlog.write('No se pudo cambiar el servidor.\n')
            else:
                tlog.write('No se encontro la key en ODBC.\n')

if __name__ == '__main__':
    main()